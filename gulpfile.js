'use strict';
 
const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const del = require('del');

const path = {
  css: "./src/css/",
  scss: "./src/scss/",
  js: "./src/js/",
  assets: "./build/assets/",
};

// Node Sass se utilizará de forma predeterminada,
// aún así se recomienda que se configure explícitamente
// para mantener una compatibilidad futura en caso de que
// cambie el valor predeterminado.
sass.compiler = require('node-sass');
 

// Compilar los ficheros SCSS y enviar el CSS resutlante
// en la carpeta /assets/css
gulp.task('sass', function () {
  return gulp.src(path.scss + '**/*.scss')
  .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: "compressed"
    }).on('error', sass.logError))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(path.css));
});



// Primero: Eliminar la carpeta /build/assets/css/
gulp.task('css-clean', function () {
  return del(path.assets + 'css');
})


//  Segundo: Copiar todos los ficheros CSS en /build/assets/css/
gulp.task('css-copy', function () {
  return gulp.src(path.css + '*.css')
    .pipe(gulp.dest(path.assets + 'css'));
});


// Vigilar si hay algún cambio en los SCSS
gulp.task('watch', function () {
  gulp.watch(path.scss + '**/*.scss', gulp.series('sass', 'css-clean'));
  gulp.watch(path.css + '*.css', gulp.series('css-copy'));
  
});


// Ejecutar gulp
gulp.task('default', gulp.parallel('css-clean', 'sass', 'watch'));